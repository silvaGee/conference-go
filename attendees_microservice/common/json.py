from json import JSONEncoder
from django.db.models import QuerySet
from datetime import datetime

class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)

class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)

class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder): #if the object we're trying to decode is in the same class as what's in the encoders model property
    encoders = {} #create an empty dictionary that will hold property names as keys and will hold property values as values
    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            for property in self.properties:  #then iterate through my list of properties, for each name in the properties list:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value #get the value of the property from the model instance & add that key-value pair to the new dictionary
            d.update(self.get_extra_data(o))
            return d  #return the dictionary
        else:
            return super().default(o)
    def get_extra_data(self, o):
        return {}
