from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo, get_weather_data


class ConferenceListEncoder(ModelEncoder): #custom modelencoder class that the COnferenceListEncoder inherits from
    model = Conference #if the object we're dealing with is a conference, we want to encode it;
    properties = ["name"]

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]
    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "starts",
        "ends",
        "description",
        "created",
        "updated",
        "max_presentations",
        "max_attendees",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method=="GET":
        conferences = Conference.objects.all()
        return JsonResponse({"conferences": conferences}, encoder=ConferenceListEncoder, safe=False)
    else:
        content = json.loads(request.body)
    # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method=="GET":
        conference = Conference.objects.get(id=id)
        weather = get_weather_data(conference.location.city, conference.location.state.abbreviation,)
        return JsonResponse(
            {"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder, safe=False
        )
    elif request.method=="DELETE": #tests if the HTTP method is delete
        count, _ = Conference.objects.filter(id=id).delete() #get the specific blog we want to delete and delete it; we use count and underscore
        #because the count is the # of things we got deleted and the underscore is just a variable we don't really use; delete returns a tuple, so we use count and the underscore to unpack it
        return JsonResponse({"deleted": count > 0}) #returns a JsonResponse indicating if something got deleted
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

# def api_show_conference(request, id):
#     conference = Conference.objects.get(id=id)
#     weather = get_weather_data(
#         conference.location.city,
#         conference.location.state.abbreviation,
#     )
#     return JsonResponse(
#         {"conference": conference, "weather": weather},
#         encoder=ConferenceDetailEncoder,
#         safe=False,
#     )




@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method=="GET":
        locations = Location.objects.all()
        return JsonResponse({"locations": locations}, encoder=LocationListEncoder)
    else:
        content = json.loads(request.body)
        #get the state object and put it in the content dictionary, but in case someone uses a state abbreviation that doesn't exist, do a try and except
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
            #instead of just giving us a 500 internal server error that says does not exist, give a better message that says it was a bad request if they use
            #a state abbreviation that doesn't exist
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        photo = get_photo(content["city"], content["state"].abbreviation)
        content.update(photo)
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method=="GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method=="DELETE": #tests if the HTTP method is delete
        count, _ = Location.objects.filter(id=id).delete() #get the specific blog we want to delete and delete it; we use count and underscore
        #because the count is the # of things we got deleted and the underscore is just a variable we don't really use; delete returns a tuple, so we use count and the underscore to unpack it
        return JsonResponse({"deleted": count > 0}) #returns a JsonResponse indicating if something got deleted
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
