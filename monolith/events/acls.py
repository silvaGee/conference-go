from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY} # Use the PEXELS API & Create a dictionary for the headers to use in the request
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search" # Create the URL for the request with the city and state
    response = requests.get(url, params=params, headers=headers) # Make the request
    content = json.loads(response.content) # Parse the JSON response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]} # Return a dict that contains a `picture_url` key & one of the URLs for one of the pictures in the response
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    params = {
        "q": f"{city}, {state}, US",
        "limit":1,
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        } # Return the dictionary
    except (KeyError, IndexError):
        return None
